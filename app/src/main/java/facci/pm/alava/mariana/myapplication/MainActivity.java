package facci.pm.alava.mariana.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText cajaCedula, cajaNombres, cajaApellidos, cajaDatos;
    Button botonLeer, botonEscribir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cajaCedula = (EditText)findViewById(R.id.txtCedulaMI);
        cajaNombres = (EditText)findViewById(R.id.txtNombresMI);
        cajaApellidos = (EditText)findViewById(R.id.txtApellidosMI);
        cajaDatos = (EditText)findViewById(R.id.txtDatosMI);

        botonLeer = (Button)findViewById(R.id.btnLeerMI);
        botonEscribir = (Button)findViewById(R.id.btnEscribirMI);

        botonLeer.setOnClickListener(this);
        botonEscribir.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEscribirMI:

                try {
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cajaCedula.getText().toString() + "," + cajaApellidos.getText().toString()+","
                                    +cajaNombres.getText().toString());
                    escritor.close();
                }catch (Exception e){
                    Log.e("Archivo MI", "ERROR ARCHIVO ESCRITURA");
                }

                break;

            case R.id.btnLeerMI:

                try {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos = lector.readLine();
                    String [] listaPersonas = datos.split(";");
                    for (int i= 0; i<listaPersonas.length; i++){
                        cajaDatos.append(listaPersonas[i].split(",")[0] + " " + listaPersonas[i].split(",")[1] + " "
                            + listaPersonas[i].split(",")[3]);
                        Log.e("aaaaaa", String.valueOf(i));
                    }
                    cajaDatos.setText("");
                    lector.close();
                } catch (Exception e) {
                    Log.e("Archivo MI", "ERROR EN LA LECTURA DEL ARCHIVO " + e.getMessage());
                }

                break;
        }
    }
}
